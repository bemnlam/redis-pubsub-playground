using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;

public class SubscribeTaskBgTask : BackgroundService
{
    private readonly ILogger _logger;
    private readonly ITaskServices _taskServices;
    private readonly ConnectionMultiplexer _connection;

    public SubscribeTaskBgTask(ILoggerFactory loggerFactory, ITaskServices taskServices)
    {
        this._logger = loggerFactory.CreateLogger<SubscribeTaskBgTask>();
        this._taskServices = taskServices;
        this._connection = ConnectionMultiplexer.Connect("localhost");
    }

    protected async override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var channel = _connection.GetSubscriber().Subscribe("messages");

        while(!stoppingToken.IsCancellationRequested)
        {
            channel.OnMessage(message =>
            {
                this._logger.LogInformation($"<< {(string) message.Message}");
            });
            await Task.Delay(5000, stoppingToken);
        }
    }
}
