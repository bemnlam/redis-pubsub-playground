using System;
using System.Threading.Tasks;
using StackExchange.Redis;

public interface ITaskServices
{

    Task<string> DoTaskAsync(ISubscriber channel);
}

public class TaskServices : ITaskServices
{
    public async Task<string> DoTaskAsync(ISubscriber channel)
    {
        // this operation should be done after some min or sec  
        var taskId = new Random().Next(1, 10000);
        
        var msg = $"Task#{taskId} published: {DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}";
        await channel.PublishAsync("messages", msg);

        return msg;
    }
}
