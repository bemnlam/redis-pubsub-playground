
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;

[ApiController]
[Route("api/tasks")]
public class TaskController : ControllerBase
{
    private readonly ILogger _logger;
    private readonly ITaskServices _svc;
    private readonly ConnectionMultiplexer _connection;

    public TaskController(ILoggerFactory loggerFactory, ITaskServices svc)
    {
        this._logger = loggerFactory.CreateLogger<SubscribeTaskBgTask>();
        _svc = svc;
        this._connection = ConnectionMultiplexer.Connect("localhost");
    }

    [HttpGet]
    public async Task<string> Get()
    {
        var channel = _connection.GetSubscriber();

        var result = await _svc.DoTaskAsync(channel);
        var msg = $">> {result}";

        _logger.LogInformation(msg);
        return msg;
    }
}
