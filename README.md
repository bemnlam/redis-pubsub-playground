# .NET Core Redis Pub/Sub playground

A simple Redis Pub/Sub example using .NET Core.

## Up and run

At root directory:

Start the local Redis server (running at localhost:5379):
```bash
docker-compose up
```

Restore, build and run the .NET Core application:
```bash
dotnet restore
dotnet build
dotnet run
```

Then, visit https://localhost:5001/api/tasks to publish a message.

At the same time, you should be able to see the message received by the subscriber and printed in the console.

## Redis Pub/Sub

- Pub: Visit `TaskController.Get()` to publish a message.
- Sub: The background service, `SubscribeTaskBgTask`, subscribed the messages when app start.

## Tools

- .NET Core (3.1.101)
- StackExchange.Redis
- Docker and docker-compose
